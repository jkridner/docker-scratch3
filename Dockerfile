FROM node:16.19-bullseye as build

ENV PORT 80

RUN apt update 
RUN apt install -y git build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev python3
RUN cd / && git clone --depth=1 --branch v1.3.2 https://git.beagleboard.org/jkridner/scratch-gui
RUN cd / && git clone --depth=1 https://git.beagleboard.org/jkridner/scratch-www

RUN cd scratch-gui \
    && npm install \
    && BUILD_MODE=dist npm run build \
    && npm link && cd ..

RUN cd /scratch-www && npm install
RUN cd /scratch-www && npm link scratch-gui
RUN cd /scratch-www && npm run build

FROM node:16.19-bullseye-slim as run

COPY --from=build /scratch-gui /

ENV PORT 8333
WORKDIR /scratch-gui
EXPOSE 8333
CMD ["npm","start"]
